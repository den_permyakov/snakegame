// Fill out your copyright notice in the Description page of Project Settings.


#include "BlockKill.h"
#include "SnakeBase.h"

// Sets default values
ABlockKill::ABlockKill()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ABlockKill::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABlockKill::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABlockKill::interact(AActor* Interactor, bool bIsHead)
{
	if(bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake)) 
		{
			Snake->Destroy(0);
		}
	}
}

